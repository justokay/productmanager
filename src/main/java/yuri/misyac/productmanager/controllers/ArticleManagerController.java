package yuri.misyac.productmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import yuri.misyac.productmanager.entity.Article;
import yuri.misyac.productmanager.entity.Category;
import yuri.misyac.productmanager.service.ArticleService;
import yuri.misyac.productmanager.service.CategoryService;

import javax.annotation.Resource;
import java.util.Map;

@Controller
public class ArticleManagerController {

    private CategoryService categoryService;
    private ArticleService articleService;

    @RequestMapping("/articleManager")
    public String articleManager(Map<String, Object> map) {
        
        map.put("category", new Category());
        map.put("article", new Article());
        map.put("categoryList", categoryService.getAllCategory());
        map.put("articleList", articleService.getAllArticle());

        return "articlesManager";
    }

    @RequestMapping(value ="/articleManager/addCategory.do", method = RequestMethod.POST)
    public String addCategory(@ModelAttribute("category") Category category) {
        
        categoryService.save(category);
        
        return "redirect:/articleManager";
    }

    @RequestMapping(value ="/articleManager/addArticle.do", method = RequestMethod.POST)
    public String addArticle(@ModelAttribute("category") Article article) {

        articleService.save(article);

        return "redirect:/articleManager";
    }

    @Resource(name = "articleService ")
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Resource(name = "categoryService")
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}
