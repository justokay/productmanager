package yuri.misyac.productmanager.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import yuri.misyac.productmanager.entity.Search;
import yuri.misyac.productmanager.service.SearchService;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.SQLException;

@Controller
public class IndexController {

    private SearchService service;
    
    @RequestMapping("index")
    public String index(ModelMap map) {
        map.put("search", new Search());

        return "index";
    }

    @RequestMapping("/")
    public String home() {
        return "redirect:/index";
    }

    @Resource(name = "searchService")
    public void setService(SearchService service) {
        this.service = service;
    }
}
