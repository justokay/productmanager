package yuri.misyac.productmanager.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import yuri.misyac.productmanager.entity.Product;
import yuri.misyac.productmanager.entity.Search;
import yuri.misyac.productmanager.entity.Store;
import yuri.misyac.productmanager.service.ProductService;
import yuri.misyac.productmanager.service.SearchService;
import yuri.misyac.productmanager.service.StoreService;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private StoreService storeService;
    private SearchService searchService;
    private List<Integer> integerList;
    
    @RequestMapping("/products")
    public String productList(Map<String, Object> map) {
        map.put("product", new Product());
        map.put("productList", productService.listProduct());
        map.put("search", new Search());
        
        return "product";
    }

    @RequestMapping("/product_view/{contactId}")
    public String productView(@PathVariable("contactId") Integer contactId,
                              Map<String, Object> map) {

        String productName = productService.getById(contactId).getProductName();
        String productDescription = productService.getById(contactId).getProductDescription();
        Integer productCount = storeService.getById(contactId).getCount();

        map.put("id", contactId);
        map.put("name", productName);
        map.put("description", productDescription);
        map.put("count", productCount);

        return "product_view";
    }

    @RequestMapping("/edit/{contactId}")
    public String edit(@PathVariable("contactId") Integer contactId,
                              Map<String, Object> map) {

        String productName = productService.getById(contactId).getProductName();
        String productDescription = productService.getById(contactId).getProductDescription();
        Integer productCount = storeService.getById(contactId).getCount();

//        map.put("id", contactId);
//        map.put("name", productName);
//        map.put("description", productDescription);
//        map.put("count", productCount);
        map.put("product", productService.getById(contactId));
        map.put("tmp", new Product());

        return "product_view";
    }

    @RequestMapping(value = "edit/editProduct", method = RequestMethod.POST)
    public String editProduct(@ModelAttribute("product") Product product) {

        String name = product.getProductName();
        String desc = product.getProductDescription();
        Product productRes = new Product();
        productRes.setId(product.getId());
        productRes.setProductName(name);
        productRes.setProductDescription(desc);
        productRes.setStore(product.getStore());

        System.out.println(productRes.getId());
        System.out.println(product.getId());

        productService.editProduct(productRes);

        return "redirect:/products";
    }
    
    @RequestMapping("delete/{contactId}")
    public String deleteContact(@PathVariable("contactId") Integer productID) {

        productService.removeProduct(productID);

        return "redirect:/products";
    }

    @RequestMapping(value = "/addProduct", method = RequestMethod.POST)
    public String addProduct(@ModelAttribute("product") Product product, BindingResult result) {

        Store store = new Store();
        store.setCount(0);

        store.setProduct(product);
        product.setStore(store);

        productService.addProduct(product);

        return "redirect:/products";
    }

    @RequestMapping(value = "/products/search.do", method = RequestMethod.POST)
    public String search(@ModelAttribute("search") Search search) {

        if (search == null) {
            return "redirect:/products";
        }

        try {
            integerList = searchService.searchTest(search.getQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/products/search";
    }

    @RequestMapping("/products/search")
    public String searchResult(Map<String, Object> map) {
        map.put("serchProduct", new Product());
        map.put("serchProductList", searchService.searchProductsByIdList(integerList));
        
        return "product_search";
    }
    
    @Resource(name = "searchService")
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }
}
