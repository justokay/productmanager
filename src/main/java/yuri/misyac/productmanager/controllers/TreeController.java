package yuri.misyac.productmanager.controllers;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import yuri.misyac.productmanager.entity.Article;
import yuri.misyac.productmanager.entity.Category;
import yuri.misyac.productmanager.entity.Node;
import yuri.misyac.productmanager.entity.RootNode;
import yuri.misyac.productmanager.service.CategoryService;
import yuri.misyac.productmanager.service.TreeService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TreeController {

    private CategoryService categoryService;
    private TreeService treeService;
    
    @RequestMapping("/tree")
    public String init() {
        return "treeExample";
    }

//    @Override
//    public byte[] build(List<ClientClaim> src) throws Exception {
//        MerchantRootModel root = new MerchantRootModel();
//        root.setMerchantList(convertToMerchantModelList(src));
//
//        JAXBContext jaxbContext = JAXBContext.newInstance(MerchantRootModel.class);
//        Marshaller marshaller = jaxbContext.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
//        marshaller.marshal(root, byteStream);
//
//        return new String(byteStream.toByteArray(), "UTF-8").replace(" standalone=\"yes\"", "").getBytes();
//    }

    @RequestMapping(value = "/tree/getXml", method = RequestMethod.GET)
    public HttpEntity<byte[]> getXml(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {

//        String xml = generateSomeXml(request);
        String xml = treeService.generateSomeXml(request);
//        System.out.println(xml);

        byte[] documentBody = xml.getBytes();

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "xml"));
        header.setContentLength(documentBody.length);
        return new HttpEntity<byte[]>(documentBody, header);
    }

    @Resource(name = "treeService")
    public void setTreeService(TreeService treeService) {
        this.treeService = treeService;
    }

    @Resource(name = "categoryService")
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}
