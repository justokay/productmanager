package yuri.misyac.productmanager.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import yuri.misyac.productmanager.entity.Article;

import java.util.List;

@Repository("articleDao")
public class ArticleDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Article article) {
        sessionFactory.getCurrentSession().save(article);
    }

    public List<Article> getAllArticle() {
        return sessionFactory.getCurrentSession().createQuery("from Article").list();
    }

    public List<Article> getArticlesByCategoryName(String categoryName) {
        return sessionFactory.getCurrentSession()
                .createQuery("select a from Article a where a.category.name=:name")
                .setParameter("name", categoryName)
                .list();
    }
}
