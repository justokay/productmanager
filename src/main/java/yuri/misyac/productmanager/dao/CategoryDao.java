package yuri.misyac.productmanager.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import yuri.misyac.productmanager.entity.Category;

import java.util.List;

@Repository("categoryDao")
public class CategoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Category category) {
        sessionFactory.getCurrentSession().save(category);
    }

    public List<Category> getAllCategory() {
        return sessionFactory.getCurrentSession().createQuery("select c from Category c").list();
    }

    public Category getCategoryById(Long id) {
        return (Category) sessionFactory.getCurrentSession().get(Category.class, id);
    }

    public List<Category> getRootCategories() {
        return sessionFactory.getCurrentSession().createQuery("select c from Category c where c.parentCategory = null").list();
    }

    public Category getCategoryByName(String currentNode) {
        return (Category) sessionFactory.getCurrentSession()
                .createQuery("select c from Category c where c.name = :name")
                .setParameter("name", currentNode)
                .uniqueResult();
    }
}
