package yuri.misyac.productmanager.entity;

import javax.persistence.*;

@Entity
@Table(name = "HS_ARTICLE")
public class Article {

    private Long id;
    private String title;
    private String text;
    private Integer catId;
    private Category category;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ARTICLE_ID", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TITLE", nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "ARTICLE_TEXT", nullable = false)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "CATEGORY_ID")
    public Integer getCategoryId() {
        return catId;
    }

    public void setCategoryId(Integer catId) {
        this.catId = catId;
    }

    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "CATEGORY_ID", insertable = false, updatable = false)
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", categoryId=" + category.getCategoryId() +
                '}';
    }
}

