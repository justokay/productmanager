package yuri.misyac.productmanager.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "HS_CATEGORY")
public class Category {

    private Long categoryId;
    private String name;
    private Set<Article> articles = new HashSet<Article>();
    private Integer parentId;
    private Category parentCategory;
    private Set<Category> subsidiariesCategories = new HashSet<Category>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CATEGORY_ID", nullable = false)
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Column(name = "CATEGORY_NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Set<Article> getArticles() {
        return articles;
    }

    public void setArticles(Set<Article> articles) {
        this.articles = articles;
    }

    @Column(name = "PARENT_ID")
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @ManyToOne
    @JoinColumn(name = "PARENT_ID", insertable = false, updatable = false)
    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentId) {
        this.parentCategory = parentId;
    }

    @OneToMany(mappedBy = "parentCategory", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Set<Category> getSubsidiariesCategories() {
        return subsidiariesCategories;
    }

    public void setSubsidiariesCategories(Set<Category> subsidiariesCategories) {
        this.subsidiariesCategories = subsidiariesCategories;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", parentId=" + (parentCategory == null ? "null" : parentCategory.getCategoryId())+
                '}';
    }
}
