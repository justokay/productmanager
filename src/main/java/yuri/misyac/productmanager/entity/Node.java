package yuri.misyac.productmanager.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"id", "text", "leaf", "cls"})
public class Node {

    private String id;

    private String text;

    private Boolean leaf;

    private String cls;

    public String getId() {
        return id;
    }

    @XmlElement(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    @XmlElement(name = "text")
    public void setText(String text) {
        this.text = text;
    }

    public Boolean getLeaf() {
        return this.leaf;
    }

    @XmlElement(name = "leaf")
    public void setLeaf(Boolean leaf) {
        this.leaf = leaf;
    }

    public String getCls() {
        return this.cls;
    }

    @XmlElement(name = "cls")
    public void setCls(String cls) {
        this.cls = cls;
    }
}
