package yuri.misyac.productmanager.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "categories")
public class RootNode {

    private List<Node> nodes;

    public List<Node> getNodes() {
        return nodes;
    }

    @XmlElement(name = "record")
    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }
}
