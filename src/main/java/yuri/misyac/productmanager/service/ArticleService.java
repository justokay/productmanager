package yuri.misyac.productmanager.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yuri.misyac.productmanager.dao.ArticleDao;
import yuri.misyac.productmanager.entity.Article;

import javax.annotation.Resource;
import java.util.List;

@Service("articleService ")
public class ArticleService {

    private ArticleDao articleDao;

    @Transactional
    public void save(Article article) {
        articleDao.save(article);
    }

    @Transactional(readOnly = true)
    public List<Article> getAllArticle() {
        return articleDao.getAllArticle();
    }
    
    @Resource(name = "articleDao")
    public void setArticleDao(ArticleDao articleDao) {
        this.articleDao = articleDao;
    }

    @Transactional(readOnly = true)
    public List<Article> getArticlesByCategoryName(String categoryName) {
        return articleDao.getArticlesByCategoryName(categoryName);
    }
}
