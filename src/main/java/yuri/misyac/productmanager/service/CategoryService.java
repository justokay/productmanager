package yuri.misyac.productmanager.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yuri.misyac.productmanager.dao.CategoryDao;
import yuri.misyac.productmanager.entity.Category;

import javax.annotation.Resource;
import java.util.List;

@Service("categoryService")
public class CategoryService {

    private CategoryDao categoryDao;

    @Transactional
    public void save(Category category) {
        if (category.getParentId() == -1) {
            category.setParentId(null);
            category.setParentCategory(null);
        } else {
            Category cat = categoryDao.getCategoryById(Long.valueOf(category.getParentId()));
            category.setParentCategory(cat);
        }
        categoryDao.save(category);
    }

    @Transactional(readOnly = true)
    public List<Category> getAllCategory() {
        return categoryDao.getAllCategory();
    }
    
    @Transactional(readOnly = true)
    public Category getCategoryById(Long id) {
        return categoryDao.getCategoryById(id);
    }

    @Transactional(readOnly = true)
    public List<Category> getRootCategories() {
        return categoryDao.getRootCategories();
    }
    
    @Resource(name = "categoryDao")
    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }
}
