package yuri.misyac.productmanager.service;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.flexible.core.QueryNodeException;
import org.apache.lucene.queryparser.flexible.standard.StandardQueryParser;
import org.apache.lucene.queryparser.surround.parser.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import yuri.misyac.productmanager.entity.Product;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;

@Component("index")
public class Indexer {

    private SessionFactory sessionFactory;
    private RussianAnalyzer analyzer = new RussianAnalyzer();
    private final static String INDEX_DIR = "/home/justokay/Downloads/index";

    public void index(Integer id) throws IOException, SQLException {
        Directory index = FSDirectory.open(new File(INDEX_DIR));
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_4_10_3, analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
        IndexWriter writer = new IndexWriter(index, config);

        Connection connection = sessionFactory.getCurrentSession().connection();
        Statement stmt = connection.createStatement();
        String sql = "select ID, PRODUCT_NAME, DESCRIPTION from PRODUCTS WHERE ID = " + id;
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            Document doc = new Document();
            
            doc.add(new TextField("id", String.valueOf(rs.getInt("ID")), Field.Store.YES));
            doc.add(new TextField("name", rs.getString("PRODUCT_NAME"), Field.Store.NO));
            doc.add(new TextField("description", rs.getString("DESCRIPTION"), Field.Store.NO));
            writer.addDocument(doc);
        }
        writer.close();
        index.close();
    }

    @Transactional
    public void deleteDocumentsFromIndex(Integer id) throws IOException, ParseException, QueryNodeException {

        Term term = new Term("id", id.toString());
        System.out.println("Deleting documents with field '" + term.field() + "' with text '" + term.text() + "'");

        Directory index = FSDirectory.open(new File(INDEX_DIR));
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_4_10_3, analyzer);
        IndexWriter writer = new IndexWriter(index, config);
        writer.deleteDocuments(term);
        writer.close();
        index.close();

    }

    @Transactional
    public void updateDocumentsFromIndex(Product product) throws ParseException, IOException, QueryNodeException, SQLException {

        deleteDocumentsFromIndex(product.getId());

        Directory index = FSDirectory.open(new File(INDEX_DIR));
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_4_10_3, analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
        IndexWriter writer = new IndexWriter(index, config);
        
        Document doc = new Document();
        doc.add(new StoredField("id", product.getId()));
        doc.add(new TextField("name", product.getProductName(), Field.Store.NO));
        doc.add(new TextField("description", product.getProductDescription(), Field.Store.NO));
        writer.addDocument(doc);
        writer.close();
    }
    
    @Resource(name = "sessionFactory")
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
