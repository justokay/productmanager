package yuri.misyac.productmanager.service;

import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.flexible.core.QueryNodeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yuri.misyac.productmanager.dao.ProductDAO;
import yuri.misyac.productmanager.entity.Product;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductDAO productDAO;
    private Indexer indexer;
    
    @Transactional
    public void addProduct(Product product) {
        productDAO.addProduct(product);

        try {
            indexer.index(product.getId());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public List<Product> listProduct() {
        return productDAO.listProduct();
    }

    @Transactional
    public void removeProduct(Integer id) {
        productDAO.removeProduct(id);

        try {
            indexer.deleteDocumentsFromIndex(id);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (QueryNodeException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void editProduct(Product product) {
        productDAO.updateProduct(product);

        try {
            indexer.updateDocumentsFromIndex(product);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (QueryNodeException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    @Transactional
    public Product getById(Integer id) {
        return productDAO.getById(id);
    }

    @Resource(name = "index")
    public void setIndexer(Indexer indexer) {
        this.indexer = indexer;
    }
}
