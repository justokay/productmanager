package yuri.misyac.productmanager.service;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yuri.misyac.productmanager.dao.ProductDAO;
import yuri.misyac.productmanager.entity.Product;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service("searchService")
public class SearchService {

    private SessionFactory sessionFactory;
    private ProductDAO productDAO;

    private static final RussianAnalyzer analyzer = new RussianAnalyzer();
    private static Directory index;

    @Transactional
    public List<Product> searchProductsByIdList(List<Integer> id) {

        List<Product> products = new ArrayList<Product>();
        
        for (Integer integer : id) {
            products.add(productDAO.getById(integer));
        }
        return products;
    }
        
    
    @Transactional
    public List<Integer> searchTest(String querystr) throws SQLException, IOException {

        List<Integer> result = new ArrayList<Integer>();

        Query q = null;
        try {
            q = new MultiFieldQueryParser(new String[]{"name", "description"}, analyzer).parse(querystr + "~1254");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int hitsPerPage = 10;

        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);
        TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);

        searcher.search(q, collector);

        ScoreDoc[] hits = collector.topDocs().scoreDocs;

        System.out.println("Found " + hits.length + " hits.");
        for (int i = 0; i < hits.length; i++) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);
            // TODO get content from DB
            System.out.println((i + 1) + "(" + d.get("id") + "). " + d.get("name") + "\t" + d.get("description"));
            result.add(Integer.valueOf(d.get("id")));
        }

        reader.close();

        return result;
    }
    
    @PostConstruct
    @Transactional
    private void indexing() throws IOException, SQLException {
        
        index = FSDirectory.open(new File("/home/justokay/Downloads/index"));

        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_4_10_3, analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        IndexWriter writer = new IndexWriter(index, config);

        Connection connection = sessionFactory.openSession().connection();
        Statement stmt = connection.createStatement();
        String sql = "select ID, PRODUCT_NAME, DESCRIPTION from PRODUCTS";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            Document doc = new Document();
            doc.add(new StoredField("id", rs.getInt("ID")));
            doc.add(new TextField("name", rs.getString("PRODUCT_NAME"), Field.Store.NO));
            doc.add(new TextField("description", rs.getString("DESCRIPTION"), Field.Store.NO));
            writer.addDocument(doc);
        }
        connection.close();
        writer.close();
    }

    @Resource(name = "productDAO")
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Resource(name = "sessionFactory")
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
