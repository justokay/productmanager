package yuri.misyac.productmanager.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yuri.misyac.productmanager.dao.CategoryDao;
import yuri.misyac.productmanager.entity.Article;
import yuri.misyac.productmanager.entity.Category;
import yuri.misyac.productmanager.entity.Node;
import yuri.misyac.productmanager.entity.RootNode;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Service("treeService")
public class TreeService {

    private CategoryDao categoryDao;
    private ArticleService articleService;
    
    @Transactional
    public String generateSomeXml(HttpServletRequest request) throws UnsupportedEncodingException {
        String node = new String(request.getParameter("node").trim().getBytes(), "UTF-8");
        System.out.println(node);
        ByteArrayOutputStream byteStream = null;
        if (node.equals("src")) {
            RootNode rootNode = new RootNode();
            rootNode.setNodes(categoryToNode(categoryDao.getRootCategories()));
            try {

                JAXBContext jaxbContext = JAXBContext.newInstance(RootNode.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                byteStream = new ByteArrayOutputStream();
                jaxbMarshaller.marshal(rootNode, System.out);
                jaxbMarshaller.marshal(rootNode, byteStream);

            } catch (JAXBException e) {
                e.printStackTrace();
            }
        } else {
            String[] split = node.split("/");
            String currentNode = split[split.length - 1];

            RootNode rootNode = new RootNode();
            rootNode.setNodes(articleToNode(
                    articleService.getArticlesByCategoryName(currentNode),
                    categoryDao.getCategoryByName(currentNode)
                    ));
            articleService.getArticlesByCategoryName("Музыка").size();
            try {

                JAXBContext jaxbContext = JAXBContext.newInstance(RootNode.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                byteStream = new ByteArrayOutputStream();
                jaxbMarshaller.marshal(rootNode, System.out);
                jaxbMarshaller.marshal(rootNode, byteStream);

            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        
        return new String(byteStream.toByteArray(), "UTF-8").replace(" standalone=\"yes\"", "");
    }

    private List<Node> articleToNode(List<Article> articlesByCategoryName, Category category) {
        List<Node> nodeList = new ArrayList<Node>();

        Set<Category> categories = category.getSubsidiariesCategories();
        if (categories.size() > 0) {
            for (Category cat : categories) {
                Node node = new Node();
                node.setText(cat.getName());
                node.setId("src/" + buildNodeId(cat));
                node.setLeaf(false);
                node.setCls("folder");
                nodeList.add(node);
            }
        }
        
        for (Article article : articlesByCategoryName) {
            Node node = new Node();
            node.setText(article.getTitle());
            node.setId("src/" + buildNodeId(article.getCategory()) + article.getTitle());
            node.setLeaf(true);
            node.setCls("file");
            nodeList.add(node);
        }
        return nodeList;
    }

    private static List<Node> categoryToNode(List<Category> categories) {
        List<Node> nodeList = new ArrayList<Node>();

        for (Category category : categories) {
            Node node = new Node();
            node.setText(category.getName());
            node.setId("src/" + buildNodeId(category));
            node.setLeaf(false);
            node.setCls("folder");
            nodeList.add(node);
        }

        return nodeList;
    }

    private static String buildNodeId(Category category) {
        if (category.getParentId() == null) {
            return category.getName();
        } else return buildNodeId(category.getParentCategory()) + "/" + category.getName();
    }

    @Resource(name = "articleService ")
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Resource(name = "categoryDao")
    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }
}
