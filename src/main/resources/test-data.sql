insert into PRODUCTS (PRODUCT_NAME, DESCRIPTION) values ('Пепси', 'Безакоголный напиток');
insert into PRODUCTS (PRODUCT_NAME, DESCRIPTION) values ('Квас', 'Безакоголный напиток');
insert into PRODUCTS (PRODUCT_NAME, DESCRIPTION) values ('Фанта', 'Безакоголный напиток');

insert into STORE (PRODUCT_ID, COUNT) values (1, 20);
insert into STORE (PRODUCT_ID, COUNT) values (2, 3);
insert into STORE (PRODUCT_ID, COUNT) values (3, 17);

insert into HS_CATEGORY (CATEGORY_NAME) values ('Музыка');
insert into HS_CATEGORY (CATEGORY_NAME) values ('Бизнес');
insert into HS_CATEGORY (CATEGORY_NAME) values ('IT');

insert into HS_ARTICLE (TITLE, ARTICLE_TEXT, CATEGORY_ID) values ('Java', 'text text', 3);
insert into HS_ARTICLE (TITLE, ARTICLE_TEXT, CATEGORY_ID) values ('C++', 'text text', 3);
insert into HS_ARTICLE (TITLE, ARTICLE_TEXT, CATEGORY_ID) values ('C', 'text text', 3);
insert into HS_ARTICLE (TITLE, ARTICLE_TEXT, CATEGORY_ID) values ('Linkin Park', 'text text', 1);
insert into HS_ARTICLE (TITLE, ARTICLE_TEXT, CATEGORY_ID) values ('Rammstain', 'text text', 1);



