<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap.min.css" />" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap-theme.min.css" />" />
    <script src="<c:url value="/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Article Manager</title>
</head>
<body>
    <div class="container" style="margin-top: 20px">
        <ul role="tablist" class="nav nav-tabs" id="myTab">
            <li class="active" role="presentation"><a aria-expanded="false" aria-controls="catman" data-toggle="tab" role="tab" id="catman-tab" href="#catman">Category Manager</a></li>
            <li role="presentation" class=""><a aria-controls="artman" data-toggle="tab" id="artman-tab" role="tab" href="#artman" aria-expanded="true">Article Manager</a></li>
            <li role="presentation" class=""><a aria-controls="null" data-toggle="tab" id="null-tab" role="tab" href="#null" aria-expanded="true">NULL</a></li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div aria-labelledby="catman-tab" id="catman" class="tab-pane fade active in" role="tabpanel">
                <form:form cssClass="form-horizontal" method="post" action="/articleManager/addCategory.do" commandName="category">
                    <fieldset>
                        <legend>Add category</legend>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="categoryName">Categore name</label>
                            <div class="col-md-4">
                                <form:input id="categoryName" cssClass="form-control input-md" path="name" placeholder="enter name..."/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="selectCategory">Set parent</label>
                            <div class="col-md-4">
                                <form:select id="selectCategory" cssClass="form-control" path="parentId">
                                    <form:option value="-1" label="--- select ---"/>
                                    <c:if test="${!empty categoryList}">
                                        <c:forEach items="${categoryList}" var="cat">
                                            <form:option value="${cat.categoryId}" label="${cat.name}"/>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-default" value="submit"/>
                            </div>
                        </div>
                    </fieldset>
                </form:form>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Category name</th>
                        <th>Parent id</th>
                        <th>Parent name</th>
                        <th>Sub categories count</th>
                        <th>Articles count</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:choose>
                        <c:when test="${empty categoryList}">
                            <p>Список пуст!</p>
                        </c:when>
                        <c:when test="${!empty categoryList}">
                            <c:forEach items="${categoryList}" var="cat">
                                <tr>
                                    <td>${cat.categoryId}</td>
                                    <td>${cat.name}</td>
                                    <c:choose>
                                        <c:when test="${cat.parentCategory == null}">
                                            <td>root</td>
                                            <td>root</td>
                                        </c:when>
                                        <c:when test="${cat.parentCategory != null}">
                                            <td>${cat.parentCategory.categoryId}</td>
                                            <td>${cat.parentCategory.name}</td>
                                        </c:when>
                                    </c:choose>
                                    <td>${fn:length(cat.subsidiariesCategories)}</td>
                                    <td>${fn:length(cat.articles)}</td>
                                </tr>
                            </c:forEach>
                        </c:when>
                    </c:choose>
                    </tbody>
                </table>
            </div>
            <div aria-labelledby="artman-tab" id="artman" class="tab-pane fade" role="tabpanel">
                <form:form cssClass="form-horizontal" method="post" action="/articleManager/addArticle.do" commandName="article">
                    <fieldset>
                        <legend>Add article</legend>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="articleName">Article name</label>
                            <div class="col-md-4">
                                <input id="articleName" name="title" placeholder="enter name..." class="form-control input-md" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="articleText">Article text</label>
                            <div class="col-md-4">
                                <textarea class="form-control" id="articleText" name="text"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="selectCategory2">Set Category</label>
                            <div class="col-md-4">
                                <form:select id="selectCategory2" cssClass="form-control" path="categoryId">
                                    <form:option value="-1" label="--- select ---"/>
                                    <c:if test="${!empty categoryList}">
                                        <c:forEach items="${categoryList}" var="cat">
                                            <form:option value="${cat.categoryId}" label="${cat.name}"/>
                                        </c:forEach>
                                    </c:if>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="submit"></label>
                            <div class="col-md-4">
                                <button id="submit" name="submit" class="btn btn-default">submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form:form>
                
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Article title</th>
                        <th>Categori</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:choose>
                        <c:when test="${empty articleList}">
                            <p>Список пуст!</p>
                        </c:when>
                        <c:when test="${!empty articleList}">
                            <c:forEach items="${articleList}" var="art">
                                <tr>
                                    <td>${art.id}</td>
                                    <td>${art.title}</td>
                                    <td>${art.category.name}</td>
                                </tr>
                            </c:forEach>
                        </c:when>
                    </c:choose>
                    </tbody>
                </table>
            </div>
            <div aria-labelledby="null-tab" id="null" class="tab-pane fade" role="tabpanel">
                <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
            </div>
        </div>
    </div>
</body>
</html>
