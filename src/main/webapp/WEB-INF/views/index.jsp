<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap.min.css" />" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap-theme.min.css" />" />
    <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>h1</title>
</head>
<body>

<div class="container">
    <ul class="list-group" style="margin-top: 20px;">
        <li class="list-group-item"><a href="products">Склад</a></li>
        <li class="list-group-item"><a href="passing_document">Покупка/продажа товара</a></li>
        <li class="list-group-item"><a href="articleManager">Articles</a></li>
        <li class="list-group-item"><a href="tree">Tree</a></li>
    </ul>
</div>

</body>
</html>
