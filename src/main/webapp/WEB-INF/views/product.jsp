<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap.min.css" />" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap-theme.min.css" />" />
    <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Склад</title>
</head>
<body>
<div CLASS="container">
    
    <div class="navbar navbar-collapse">
        <div class="col-sm-1 col-md-6 pull-left">
            <label for="addForm">Добавить новый товар:</label>
            <form:form id="addForm" cssClass="form-inline" role="form" method="post" action="addProduct" commandName="product">
                <div class="form-group">
                    <form:input cssClass="form-control" path="productName" placeholder="Product name"/>
                </div>
                <div class="form-group">
                    <form:input cssClass="form-control" path="productDescription" placeholder="Product description"/>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form:form>
        </div>
        <div class="col-lg-3 pull-right">
            <form:form cssClass="navbar-form" method="post" action="products/search.do" commandName="search" role="search">
                <div class="input-group">
                    <%--<input type="text" class="form-control" placeholder="Search for...">--%>
                    <form:input cssClass="form-control" path="query" placeholder="Search for..."/>
                    <span class="input-group-btn">
                        <input class="btn btn-default" type="submit" value="Search"/>
                    </span>
                </div><!-- /input-group -->
            </form:form>
        </div>
    </div>
    <div class="container">
        <c:choose>
            <c:when test="${empty productList}">
                <p>Список пуст!</p>
            </c:when>
            <c:when test="${!empty productList}">
                <label for="productList">Список товаров:</label>
                <table id="productList" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Идентификатор</th>
                        <th>Название товара</th>
                        <th>Описание товара</th>
                        <th>Количество</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${productList}" var="product">
                        <tr>
                            <td>${product.id}</td>
                            <td>${product.productName}</td>
                            <td>${product.productDescription}</td>
                            <td>${product.store.count}</td>
                            <td>
                                <a class="btn btn-primary" href="edit/${product.id}">edit</a>
                                <a class="btn btn-danger" href="delete/${product.id}">delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
        </c:choose>
    </div>
    <ul class="menu">
        <li><a href="index">Главная</a></li>
        <li><a href="passing_document">Покупка/продажа товара</a></li>
    </ul>
</div>
</body>
</html>