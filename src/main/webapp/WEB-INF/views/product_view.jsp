<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap.min.css" />" />
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap-theme.min.css" />" />
    <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Edit</title>
</head>
<body>

<div class="container">
        <h1>produxt_view</h1>
        <a href="/">--> main</a>
        
        <%--<form:form id="addForm" cssClass="form-inline" role="form" method="post" action="editProduct" commandName="product">--%>
        <form class="form-horizontal" action="editProduct" method="post">
            
            <fieldset>

                <legend>Edit product id=${product.id}</legend>
                <input name="id" type="text" value="${product.id}"/>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="productName">Name</label>
                    <div class="col-md-4">
                        <input id="productName" name="productName" type="text" placeholder="product name" class="form-control input-md" value="${product.productName}">
                        <%--<form:input id="productName" name="productName" cssClass="form-control input-md" path="productName" placeholder="product name"/>--%>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">Description</label>
                    <div class="col-md-4">
                        <input id="description" name="productDescription" type="text" placeholder="description" class="form-control input-md" value="${product.productDescription}">
                        <%--<form:input id="productName" name="productName" cssClass="form-control input-md" path="productName" placeholder="product name"/>--%>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description"></label>
                    <div class="col-md-4">
                        <button id="firstbutton" type="submit" name="firstbutton" class="btn btn-primary">Save</button>
                        <%--<button id="secondbuttom" name="secondbuttom" class="btn btn-default">Cancle</button>--%>
                    </div>
                </div>

            </fieldset>
        </form>
        <%--</form:form>--%>

</div>
</body>
</html>