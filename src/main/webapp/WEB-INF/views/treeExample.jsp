<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<c:url value="/extjs/resources/css/ext-all.css" />" />
    <script type="text/javascript" src="<c:url value="/extjs/ext-all.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/jsapp/app.js"/>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title>Example</title>
</head>
<body>
</body>
</html>
