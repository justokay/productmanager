Ext.define('TR.controller.Countries', {
    extend : 'Ext.app.Controller',

    //define the stores
    stores : ['Countries'],
    //define the models
    models : [],
    //define the views
    views : ['CountryTree'],
    refs: [{
        ref: 'myCountryTree',
        selector: 'countryTree'
    }
    ],

    init : function() {
        this.control({

            'viewport' : {
                render : this.onPanelRendered
            },
            'countryTree button[text=Expand All]' : {
                click : this.expandAll
            },
            'countryTree button[text=Collapse All]' : {
                click : this.collapseAll
            },
            'countryTree' : {
                itemclick : this.treeItemClick,
                itemappend : this.treeItemAppend,
                iteminsert : this.treeItemInsert,
                itemremove : this.treeItemRemove,
                itemmove : this.treeItemMove,
                beforeiteminsert: this.treeBeforeItemInsert

            }
        });
    },

    onPanelRendered : function() {
        //just a console log to show when the panel si rendered
        console.log('The panel was rendered');
    },

    expandAll : function() {
        //expand all the Tree Nodes
        var myTree = this.getMyCountryTree();
        myTree.expandAll();
    },

    collapseAll : function() {
        //expand all the Tree Nodes
        var myTree = this.getMyCountryTree();
        myTree.collapseAll();
    },

    treeItemClick : function(view, record) {
        //some node in the tree was clicked
        //you have now access to the node record and the tree view
        Ext.Msg.alert('Clicked on a Tree Node',
            'Node id: ' + record.get('id') + '\n' +
            'Node Text: ' + record.get('text') + '\n' +
            'Parent Node id: ' + record.get('parentId') + '\n' +
            'Is it a leaf?: ' + record.get('leaf') + '\n' +
            'No of Children: ' + record.childNodes.length
        );
        //now you have all the information about the node
        //Node id
        //Node Text
        //Parent Node
        //Is the node a leaf?
        //No of child nodes
        //......................
        //go do some real world processing
    },

    treeItemAppend : function(object, node, index) {
        console.log('a new child node is appended');
        console.log(object,node,index);
    },

    treeItemInsert : function(object, node, refNode) {
        console.log('a new child node is inserted');
        console.log(object,node,refNode);
    },

    treeItemRemove : function(object, node) {
        console.log('a child node is removed');
        console.log(object,node);
    },

    treeItemMove : function(object, oldParent, newParent, index) {
        console.log('node is moved to a new location in the tree');
        console.log(object,oldParent,newParent,index);
    },

    treeBeforeItemInsert : function(object, node, refNode) {
        console.log('before a node is added to the tree');
        console.log(object,node,refNode);
    }
});