Ext.define('TR.store.Countries', {
    extend: 'Ext.data.TreeStore',
    proxy: {
        type: 'ajax',
        //url : 'jsapp/store/CountryData.xml',
        url : 'tree/getXml',
        extraParams: {
            isXml: true
        },
        reader: {
            type: 'xml',
            root: 'categories',
            record: 'record'
        }
    },
    sorters: [{
        property: 'leaf',
        direction: 'ASC'
    },{
        property: 'text',
        direction: 'ASC'
    }],
    root: {
        text: 'root',
        id: 'src',
        expanded: true
    },
    folderSort: true

});